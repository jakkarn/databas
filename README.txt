Overview:
	This is a e-commerce site used for a course on Luleå University of 
	Technology. It is a practice project that has a focus on learning 
	the basics of building a database in MySQL.

	The code is a mess. It would be nice to re-write all code in a re-used
	fashion. There are ever places where functions that exist aren't used
	but actually just copy-pasted.

	The stored procedure "conduct_order" suddenly didn't give
	the error message it was supposed to give. Not fixed.

	Most files are used, but some are not used at all. There was not enough
	time to make structure in this code. 

Essential files:
	index.php 		- 
	comment.php		- rating and comments
	checkout.php	- shopping cart and ordering
	signup.php		- creating an account
	functions.php	- reused functions for SQL, php and a little javascript

admin tools:
	administrator.php

some notes on peculiar code:
	- $_SESSION['message'] is used in combo with php-function popup()
	to make a javascript popup-window appear even when refreshing.
	- CSS is written quite horribly in the beginning of some files,
	not even sure if it is serving a purpose anymore.