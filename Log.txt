2015-11-03
	GIT-REPOSITORY
		- chose bitbucket, since easier to keep private.
		- fixed a repository
	SQL
		- installed postgreSQL. Course book read that it was good for 
		relational databases.
		- what is MySQLworkbench for? just GUI?
		- how do I actually code database?
		- where are the database-files saved?
		- how to upload?
	WEBSITE
		- What is neccesary to support SQL, and postgreSQL?
		- PHP is on server side, Javascript on client side.
		so we guess PHP is safer for a e-commerce site.
		- PHP is used for interaction with SQL

2015-11-04
	PHP and FRONTPAGE inför första labpasset
		- Jakob läser på om hur man kopplar ihop PHP med SQL
		- Niclas kollar upp att skapa framsidan via programmet FRONTPAGE

2015-11-05
	Testing out mySQL on server
		- created a small html form
		- add and lookup values in test database tested
		- MySQLworkbench seems good for creating databases
		- so far chive same as MySQLworkbench but little worse interface(-) 
		and in browser(+)
		- frontpage gives pure HTML code that is quite messy, but probably 
		efficient when used to it.
2015-11-10
	SQLing
		- Learned about using SQL-commands to interact with databases.
	Frontpage dismissed!
		- Realized frontpage wasn't so good, switched to another software.
	Making a demo
		- Created 3 html forms for "adding", "removing" and "looking up"
		- Implemented the html forms with SQL-commands in a messy php-script

2015-11-12
	Report
		- Only clarified the documentation today
		- Much time has gone to other courses

2015-11-24
	DESIGN
		Fundamental parts:
			- users
				functions:
					login to site
					add to cart/clearcart
					buy cart
				data:
					ID
					name
					password
					phone
					email
			- shopping cart
				functions:
					purchase order
						filling out order 
						agreement
						connected wow-account
					clearing
					adding
					removing
				data:
					*customer_ID
					*product_ID
			- products
				functions:
					ammount of gold
					show in list
					rate
					description
				data:
					ID
					name
					rating
					*customer_ID
					description string
			- rating
				functions:
					click-rating 1-tao
					comment
			- orders
				data:
					*customer
					*products
					date
					status

		Bonus parts
			- different categories of products
				weapons
				mounts
				cats
			- search
	STORIES
		P
		1 Graphical - graphical webpage for interaction with webpage
		2 relational database with all parts
		3 place orders
			- add to cart
			- purchase button
			- see that the order exist
		3.2 report thoroughly
		4 login
		5 rating with comment
		6 better shopping cart
		7 better rating
		8 Bonus parts
	About the work:
		- 	We don't know anything about databases, must read more about it.
		- 	Created a weapons table that has a foreign key to an owner
			which of course is the name of an acocunt. It was hard to understan
			what was what in MySQLworkbench but it gave some understanding to 
			how a foreign key worked.