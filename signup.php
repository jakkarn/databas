<?php
	session_start();
?>

<html>

<head>
<meta http-equiv="Content-Language" content="sv">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>GoldFarm4U - Signup</title>
<style type="text/css">
	#c{	text-align: center;	}
	#l{	text-align: left;	}
	#r{	text-align: right;	}
</style>
</head>

<body>

<?php
	include('/afs/ltu.se/systemdata/www/students/nicvre-3/functions.php');
?>

<p align="center">
	<a href="index.php">
		<img border="0" src="images/banner0.png" width="1175" height="161">
	</a>
</p>

<hr>
<!--
<audio loop autoplay>
	<source src="music/bossbattle.mp3" type="audio/mpeg">
	Your browser does not support the audio element.
</audio>
-->
<form method="post">

			<h1 id="c">Please fill this form</h1>

	<table width="500" align="center" border = "0" cellpadding="0" cellspacing="5" bgcolor="#FFFFFF">
		<tr>
			<td id="l">* Full name(ex John Jonsson)</td>
			<td><input type="text" name="name" size="30" required></td>
		</tr>
		<tr>
			<td id="l">* E-mail</td>
			<td><input type="text" name="email" size="30" required></td>
		</tr>
		<tr>
			<td id="l">Phone number(ex +46700737134)</td>
			<td><input type="int" name="phone" size="30"></td>
		</tr>
		<tr></tr>
		<tr>
			<td id="l">* Country</td>
			<td><input type="text" name="country" size="30" required></td>
		</tr>
		<tr>
			<td id="l">* Adress</td>
			<td><input type="text" name="address" size="30" required></td>
		</tr>
		<tr>
			<td id="l">* Postal code</td>
			<td><input type="int" name="postalcode" size="30" required></td>
		</tr>
		<tr></tr>
		<tr>
			<td id="l">* Desired Username</td>
			<td><input type="text" name="username" size="30" required></td>
		</tr>
		<tr>
			<td id="l">* Password</td>
			<td><input type="password" name="password" size="30" required></td>
		</tr>
		<tr>
			<td id="l">* Repeat Password</td>
			<td><input type="password" name="passwordrep" size="30" required></td>
		</tr>
		<tr>
			<td></td>
			<td id ="c"><input type="submit" value="Register!" name="register"></td>
		</tr>
	</table>
	<p id="c"><b>Fields marked with * must be filled in</b></p>
</form>

<?php
	$conn = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE");

	if (!$conn) { 
		die('Could not connect: ' . mysql_error());  
		echo $conn->error;
	}

	if(isset($_GET['Message'])){
		popup($_GET['Message']);
	}

	if(isset($_POST['register'])){
		$stmt = $conn->prepare('CALL add_user(?,?,?,?,?,?,?,?)');
		$stmt->bind_param('ssssssss', 
			$username, 	$email, 		$name, 
			$address, 	$postalcode, 	$country, 
			$phone, 	$password
			);

		$username 	= $_POST["username"];
		$email		= $_POST["email"];
		$name 		= $_POST["name"];
		$address 	= $_POST["address"];
		$postalcode = $_POST["postalcode"];

		$country 	= $_POST["country"];
		$phone 		= $_POST["phone"];
		$password 	= $_POST["password"];
		$passwordrep= $_POST["passwordrep"];

        	if($username == "" 
			or $email == "" 
			or $name == "" 
			or $address == "" 
			or $postalcode == "" 
			or $country == "" 
			or $password == "") { 
        	header('Location: signup.php?Message='.urlencode('All fields must be filled!'));
		}
		else if($password != $passwordrep){
			header('Location: signup.php?Message='.urlencode('password missmatch'));
		}
		else{ 

			$stmt->execute();
			
			#$sql = "CALL add_user('$username','$email', '$name','$address', $postalcode,'$country',$phone,'$password')"; 
			#mysqli_query($conn,$sql);             
			if($conn->error){ 
				header('Location: signup.php?Message='.urlencode($conn->error));
			}       
			else{ 
				$_SESSION['A_C'] = "Account created!";
				header('Location: index.php');
			}

			//this had to be closed last
			$stmt->close();
		}
	}
	echo "</table>";      
	mysqli_close($conn);
?>

</body>
</html>


