<html>

<head>

<style type="text/css">
	#topimage{	
		position:fixed;
		width=1000px;
		left:50%; 
		margin-left:-600px; 
		height=150px;
		z-index:-1;
	}
	#l{	text-align: left;	}
	#r{	text-align: right;	}
</style>

</head>

<?php
	if(!isset($_SESSION)){
		session_start();	
	}
	
	function refresh(){
		header('Location: '.$_SERVER['REQUEST_URI']);	
	}	
	
	function popup(){
		if(isset($_SESSION['message']) and $_SESSION['message'] != 'no message'){
			echo 	'<script type="text/javascript">'
					,	'alert("'.$_SESSION['message'].'");'
					,'</script>';
			$_SESSION['message'] = 'no message';
		}
	}

	//return the current userID
	function userID(){
		$conn = new mysqli("SERVERNAME", "USERNAME", "PASSWORD", "DATABASE");
		if (!$conn) {
			die('Could not connect: '.mysql_error());
			popup($conn->error);
		}

		$un = $_SESSION['username'];
		$temp = $conn->query("SELECT id FROM customers WHERE username='$un'");
		$temp = $temp->fetch_assoc();
		$id = $temp['id'];
		$conn->close();
		return $id;
	}

	function get_username($id){
		$conn = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE");  
		$result = $conn->query("SELECT name FROM customers WHERE id=".$id);
		$row = $result->fetch_assoc();
		$name  =  $row['name'];
		$conn->close();
		return $name;
	}

	//return name of current user
	//function real_name($username){}


	//returns a list of all items in cart
	function cart_list($username){
		$id=userID($username);
		$conn = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE"); 
		if (!$conn) { 
			die('Could not connect: ' . mysql_error());  
			echo "$conn->error";
		}
		$list = $conn->query("SELECT * FROM carts where customer_id=".$id."");
		echo $conn->error;
		return $list;
		$conn->close();
	}

	// count the amount of items in cart and return it
	function count_cart($username){
		$conn = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE"); 
		$list = cart_list($username);
		$items = 0;
		while($item = $list->fetch_assoc()){
			$items+=$item['amount'];
		}
		if($conn->error){
			echo "$conn->error";
		}
		else{
			$conn->close();
			return $items;
		}
		$conn->close();
	}

	function count_cart_rows($username){
				$conn = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE"); 
		$list = cart_list($username);
		$items = 0;
		while($item = $list->fetch_assoc()){
			$items+=1;
		}
		if($conn->error){
			echo "$conn->error";
		}
		else{
			$conn->close();
			return $items;
		}
		$conn->close();
	}
	
	// average rating of product	
	function get_rating($product_id){
		$conn = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE"); 
		$result = $conn->query("SELECT SUM(rating) AS rating_sum FROM ratings WHERE product_id=".$product_id);
		$row = $result->fetch_assoc();
		$rating =  $row['rating_sum'];  
		$result = $conn->query("SELECT COUNT(rating) AS raiting_count FROM ratings WHERE product_id=".$product_id." AND rating IS NOT NULL");
		$row = $result->fetch_assoc();
		$count  =  $row['raiting_count'];
		if($count <= 0){
			$avg_rating = 'not rated. BE THE FIRST!'; 
		}
		else{
			$avg_rating = $rating/$count;
		}
		$conn->close();
		return round($avg_rating, 1);
	}
	// print a full list of all items in user's cart
	// and return 
	function print_cart_list($username){
		$list=cart_list($username);
		$totalPrice=0;
		if($list->num_rows > 0){
			while($row =  $list->fetch_assoc()){
				$id = $row['product_id'];
				$product = get_product($id);
				$product_price = $product['price']*$row['amount'];
				if($row['amount']){
					echo "<br><hr>";
					echo $row['amount']." x <i>".$product['name']."</i> - (".$product_price." euro)";
				}
				$totalPrice += $product_price;
			}
			//echo "TOTAL: ".$totalPrice." euro";
		}
		else{
			echo "No items in cart :'(";
		}
		return $totalPrice;
	}

	// return name of product from product table based on product id
	function get_product($id){
		$conn 	 = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE");
		$sql  	 = "SELECT name, price, instock FROM products WHERE id=".$id;
		$product = $conn->query($sql);
		$product = $product->fetch_assoc();
		$conn->close();
		return $product;
	}

	// return name of product
	function get_product_name($id){
		$product = get_product($id);		
		return $product['name'];
	}

	// return price of product
	function get_product_price($id){
		$product = get_product($id);
		return $product['price'];
	}

	// return how many in stock
	function get_product_instock($id){
		$product = get_product($id);
		return $product['instock'];
	}

	// return result of query for ALL products in database
	function get_all_products(){
		$conn 	 = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE");
		$sql  	 = "SELECT * FROM products";
		$products = $conn->query($sql);
		$conn->close();
		return $products;
	}

	// return result of all rows with one product_id
	function get_ratings($product_id){
		$conn 	 = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE");
		$sql  	 = "SELECT * FROM ratings WHERE product_id=".$product_id;
		$ratings = $conn->query($sql);
		$conn->close();
		return $ratings;
	}

	// clears users shopping cart
	function clear_cart($username){
		$conn 	 = new mysqli("SERVER", "USERNAME", "PASSWORD", "DATABASE");
		$result = $conn->query("SELECT id FROM customers WHERE username='$username'");
		if( $result->num_rows == 1){
			$row = $result->fetch_assoc();
			$sql = "CALL remove_all_in_cart(".$row['id'].")";
			$conn->query($sql);
		}
		$conn->close();
	}
?>


<body>

</body>
</html>
