<?php 
function kill_all_procedures(){
	$conn = new mysqli("SERVERNAME", "USERNAME", "PASSWORD", "DATABASENAME");
	if (!$conn) {
		die('Could not connect: '.mysql_error());
		popup($conn->error);
	}

	$result = $conn->query("SHOW FULL PROCESSLIST");
	while ($row=$result->fetch_array()) {
		$process_id=$row["Id"];
		$sql="KILL $process_id";
		$conn->query($sql);
	}
	$conn->close();
	session_write_close();
}
?>


<div style="position:fixed; width:320px; top:30%; left:65%;">
	<form method="post">
		<br>ADMINISTRATOR SQL:<br>
		<input width="300" type="text" name="admin_sql"><br><br>
		<input type="submit" name="admin" value="send query">
		<input type="submit" name="KILL_PROCEDURES" value="KILL PROCEDURES!">
	</form>
	<br>
	<div style="position:fixed; overflow:scroll; height:400px; width:320px;">
		<?php
			if( isset($_POST['admin']) and isset($_POST['admin_sql']) ){
				echo "<b>Command sent</b>:<br> ".$_POST['admin_sql']."<br><br>";
				$admin_result=$conn->query($_POST['admin_sql']);
				if($conn->error){
					popup($conn->error);
				}
				else{
					if($admin_result->num_rows > 0){
						while($admin_row = $admin_result->fetch_assoc()){
							foreach ($admin_row as $column) {
								echo $column." ";
							}
							echo "<br><br>";
						}
						echo $conn->error;
					}
					else{
						echo "no rows returned";
						echo "<br><br>".$conn->error;
					}
				}
			}
			else if(isset($_POST['admin']) and isset($_POST['KILL_PROCEDURES'])){
				kill_all_procedures();
			}
		?>
	</div>
</div>